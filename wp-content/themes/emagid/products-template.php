<?php
/*
 * Template Name: Products Template
 */

get_header(); ?>

    <div class="hero_front">
        <h1><?php wp_title(''); ?></h1>
        
        <div class="product_options">
            <a href="/products/supplements">
                <div class="product_select">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_pill.png">
                    <h5>Supplements</h5>
                </div>
            </a>
            <a href="/products/beauty">
                <div class="product_select">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_leaf.png">
                    <h5>Beauty</h5>
                </div>
            </a>
            <a href="/products/food">
                <div class="product_select">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_apple.png">
                    <h5>Food</h5>
                </div>
            </a>
            
        </div>
    </div>
</div>
	<div class="page_container" id="home">
        
            
            <div class="page_content">
                
                
                <div class="mid_layer">
                    <div class="wrapper">
                        
                    <div class="product_grid">
                        <div class="product_select">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/product_supp.png">
                            <h5>Multivitamins</h5>
                            <p>$29.99</p>
                        </div>
                        <div class="product_select">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/product_supp.png">
                            <h5>Multivitamins</h5>
                            <p>$29.99</p>
                        </div>
                        <div class="product_select">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/product_supp.png">
                            <h5>Multivitamins</h5>
                            <p>$29.99</p>
                        </div>
                        <div class="product_select">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/product_supp.png">
                            <h5>Multivitamins</h5>
                            <p>$29.99</p>
                        </div>                
                    </div>
		
                </div>
                    
                 
            </div>

        </div>

	</div><!-- #primary -->


<script>
$(document).ready(function(){
  $('.your-class').slick({
//    setting-name: setting-value
  });
});
		
</script>

<?php
get_footer();
