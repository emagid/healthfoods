<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>
    <div class="hero_front">
        <h1><?php the_field('hero_title'); ?></h1>
        <p>Choose one of our healthy options below</p>
        
        <div class="product_options">
            <a href="/product-category/vitamins-supplements/">
                <div class="product_select main">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_pill.png">
                    <h5>Supplements</h5>
                </div>
            </a>
            <a href="/product-category/natural-beauty-and-skin/">
                <div class="product_select main">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_leaf.png">
                    <h5>Beauty</h5>
                </div>
            </a>
            <a href="/product-category/food-beverage/">
                <div class="product_select main">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_apple.png">
                    <h5>Food</h5>
                </div>
            </a>
            
        </div>
    </div>
</div>

	<div class="page_container" id="home">
        
            
            <div class="page_content">
                <div class="wrapper">
                    <div class="top_layer">
                        <h1>Why 1 Bay Ridge Health Food</h1>
                        <?php the_field('content'); ?>
                    </div>
                </div>
                
                
                <div class="mid_layer">
                    <div class="wrapper">
                    <h1>Featured Products</h1>
                        
    
                        <div class="product_grid">
                            <ul>
<?php
$args = array(
    'post_type' => 'product',
    'posts_per_page' => 4,
    'tax_query' => array(
            array(
                'taxonomy' => 'product_visibility',
                'field'    => 'name',
                'terms'    => 'featured',
            ),
        ),
    );
$loop = new WP_Query( $args );
if ( $loop->have_posts() ) {
    while ( $loop->have_posts() ) : $loop->the_post();
        wc_get_template_part( 'content', 'product' );
    endwhile;
} else {
    echo __( 'No products found' );
}
wp_reset_postdata();
?>
                                </ul>
                    </div>
		
                </div>
            </div>
                
            <div class="lower_level">
                <div class="ig_grid">
                    	
<?php echo do_shortcode('[instagram-feed]'); ?>
                    
                </div>
            </div>
        </div>

	</div><!-- #primary -->


<script>
$(document).ready(function(){
  $('.your-class').slick({
//    setting-name: setting-value
  });
});
		
</script>

<?php
get_footer();
