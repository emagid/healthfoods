<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/font/font.css">

<!--    SLICK-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="header_wrapper" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/home_hero_mini.jpg)">

	<header>
		
        <div class="header_container">
            <div class="header_nav">
	 		<?php 
		wp_nav_menu(array(
		"theme_location" => "left-menu"
    )); ?>
            </div>
            <div class="header_branding">
                <a href="/">
                    <h1 class="site_title">1 Bay Ridge Health Food</h1>
                </a>
            </div>
            <div class="header_links">
	 		<?php 
		wp_nav_menu(array(
		"theme_location" => "right-menu"
    )); ?>
            </div>
                            <div class="search_field">
                    <?php echo do_shortcode('[wpbsearch]'); ?>
                </div>
        </div>

	</header>
    

