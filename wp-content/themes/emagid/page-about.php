<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>

    <div class="hero_front">
        <h1><?php wp_title(''); ?></h1>
    </div>
</div>

	<div class="page_container" id="home">
        
            
            <div class="page_content">
                <div class="wrapper">
                    <div class="top_layer">
                        <h1>About 1 Bay Ridge Health Food</h1>
                        <?php the_field('content'); ?>

                    </div>
                </div>
                
                
            <div class="lower_level">
                <div class="ig_grid">
                    <?php echo do_shortcode('[instagram-feed]'); ?>
                </div>
            </div>
        </div>

	</div><!-- #primary -->


<?php
get_footer();
